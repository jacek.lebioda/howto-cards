mkdir -p public
artefact="latest"

# loop through all the branches
for branch in $(git for-each-ref --format='%(refname:strip=3)' refs/remotes); do
    echo $branch;
    if [[ $branch == "master" ]]; then
        artefact="stable";
    elif [[ $branch == "develop" ]]; then
        artefact="latest";
    else
        artefact="unstable";
    fi
    echo $artefact;
    git checkout -f $branch
    git reset --hard $branch

    # build the website
    bundle exec jekyll build --baseurl="howto-cards/$artefact" -d "public/$artefact"
done

# checkout the current branch
echo $CI_COMMIT_REF_NAME
git checkout -f $CI_COMMIT_REF_NAME
git reset --hard origin/$CI_COMMIT_REF_NAME

# set the auto redirection
cp .ci/.autoRedirect public/index.html
