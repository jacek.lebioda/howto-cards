---
layout: page
title: Help
permalink: /the-help/
order: 3
---

If you need help, please create a request for support by creating a ticket on [ServiceNow](https://service.uni.lu/) using the catalog item _LCSB/BioCore: Application Services_.

If you don't have access to service now, please contact us using <span id="obf"><noscript><span style="unicode-bidi:bidi-override;direction:rtl;">ul.inu@snimdasys-bscl</span></noscript></span>.


<script>document.getElementById("obf").innerHTML="<n uers=\"znvygb:ypfo-flfnqzvaf@hav.yh?fhowrpg=[ynoPneqf] Freivpr erdhrfg\" gnetrg=\"_oynax\">guvf rznvy</n>".replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);});document.body.appendChild(eo);</script>