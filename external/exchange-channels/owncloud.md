
# Owncloud Quickguide

**LCSB ownCloud**  is a private cloud storage service for the use of LCSB staff and collaborators. It is suitable for exchanging small-sized files (up to 1-2 gigabyte). All communication with the LCSB ownCloud server is SSL encrypted.


## Obtaining a LUMS account

A LUMS (LCSB User Management System) account is needed in order to use LCSB ownCloud. LUMS accounts for LCSB staff are normally created within the first few days of starting work at the LCSB. In addition to staff, researchers at Partner Institutes may be given LUMS accounts. If you want to create a LUMS account, or require support, you should contact [LCSB admin team](mailto:lcsb-sysadmins@uni.lu).

## Using LCSB ownCloud

Similar to other cloud storage systems, ownCloud is accessible both via a browser and also via a client application. On the web, LCSB's ownCloud is at [https://owncloud.lcsb.uni.lu/](https://owncloud.lcsb.uni.lu/)



![LCSB ownCloud web login](../../external/img/owcld_1.png)

You can download the ownCloud client suitable for your staff computer [here](https://owncloud.org/download/). User documentation on ownCloud tools and portal can be found [here](https://doc.owncloud.com/server/index.html).

When sharing research data, one should observe the following guidance using Owncloud:

* Limit folder shares to only the personnel that needs to access data.

* When sharing via Links, always set a **password** and an **expiration date** for the link.



  ![](../../external/img/owcld_2.png)


* When sharing folders via Links, **the link passwords should not be sent plain in email**. Instead, password links should be used. If you need to generate link passwords can use the [Private bin](https://privatebin.lcsb.uni.lu) service hosted by the LCSB.


 **IMPORTANT NOTE: Whenever the LCSB shares an ownCloud folder with you (collaborator), the share password will be reachable via link, which will expire. Therefore you should make a record of your password once you view it.**
