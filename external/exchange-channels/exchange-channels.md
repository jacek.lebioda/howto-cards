---
layout: page
permalink: /external/exchange-channels/
---
# Exchanging Research Data with Collaborators
{:.no_toc}

LCSB provides two channels for the exchange of data with research collaborators **ownCloud** and **Data Upload Manager (DUMA)**. This howto-card provides information on the use of these exchange channels.

* TOC
{:toc}

{% include_relative owncloud.md %}
{% include_relative duma.md %}

